from requests import post, models


class EmptyEpsilonServer:
    def _get_url(self):
        url = "http://"
        url += str(self.ip)
        url += ":"
        url += str(self.port)
        url += "/"
        return url

    def __init__(self, ip, port):
        self.ip = ""
        self.port = 8080
        self.set_server(ip, port)

    """ Sets IP and port of the server 
    
    :param ip = IP address (or hostname) of server
    :param port = port on which Empty Epsilon server listen on
    """
    def set_server(self, ip, port):
        self.ip = ip
        self.port = port

    def exec_lua(self, lua_string):
        response = post(self._get_url() + "exec.lua", lua_string)
        data = models.Response.json(response)
        return data
