from flask import Flask, render_template, Blueprint, request, url_for
from flask import current_app as app
from flask_menu import register_menu

playerShipData = Blueprint('playerShipData', __name__)


@playerShipData.route('/')
@register_menu(playerShipData, ".playerShipData", "Player ships data")
def player_ships_data():
    lua_string = """
ships = {}

function noop()
return
end

for i=1, 32 do
  player = getPlayerShip(i)
  if player == nil then
    noop()
  else
    ships[i] = player:getCallSign()
  end
end

return ships
    """
    response = app.emptyEpsilonServer.exec_lua(lua_string)
    return render_template('playerShipData/playerShipData.html', result=response)

