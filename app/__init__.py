#Import flask and template operators
from flask import Flask, render_template, redirect, url_for, request, render_template_string
from flask_menu import Menu, register_menu
from .EmptyEpsilonServer import EmptyEpsilonServer

from .modules.playerShipData import playerShipData


# Define the WSGI application object
app = Flask(__name__, template_folder='templates', static_url_path='/static', static_folder='static')

# Configurations that must be set
app.config["EE_HTTP_API_IP"] = ""
app.config["EE_HTTP_API_PORT"] = None

# Configurations from config file
app.config.from_object('config')

app.emptyEpsilonServer = EmptyEpsilonServer(app.config["EE_HTTP_API_IP"], app.config["EE_HTTP_API_PORT"])

Menu(app=app)

# Register all modules blueprints:
app.register_blueprint(playerShipData, url_prefix='/playerShipData/')


@app.after_request
def add_header(r):
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


@app.context_processor
def inject_dict_for_all_templates():
    ret_dict = {
        "server_ip": app.config["EE_HTTP_API_IP"],
        "server_port": app.config["EE_HTTP_API_PORT"]
    }
    return ret_dict


@app.route('/')
def index():
    return render_template('layout.html')


@app.route('/set_server', methods=['POST'])
def set_server_process():
    app.config["EE_HTTP_API_IP"] = str(request.form["server_ip"])
    app.config["EE_HTTP_API_PORT"] = int(request.form["server_port"])
    app.emptyEpsilonServer.set_server(app.config["EE_HTTP_API_IP"], app.config["EE_HTTP_API_PORT"])
    return redirect(url_for(".index"))


@app.route('/set_server')
@register_menu(app, ".set_server", "Change server")
def set_server():
    return render_template_string("""
    {% extends "layout.html" %}
    {% block title %}Change server{% endblock %}
    {% block content %}
        {% include 'get_server.html' %}
    {% endblock %}
    """)



# Error handling

@app.errorhandler(404)
def not_found(error):
    return render_template('errors/404.html'), 404
